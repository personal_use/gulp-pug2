const gulp = require('gulp');
const config = require('./gulp-settings.json');
const clean = require('del');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();
const newer = require('gulp-newer');
const imagemin = require('gulp-imagemin');
const pngmin = require('gulp-pngmin');
const copy = require('gulp-contrib-copy');
const gcmq = require('gulp-group-css-media-queries');
const wait = require('gulp-wait');
const csscomb = require('gulp-csscomb');
const pug = require('gulp-pug2');

const postCssPlugins = [
  autoprefixer({ browsers: ['last 1 versions'] })
];

gulp.task('views:compile', () => gulp.src(`${config.devDir}*.pug`)
    .pipe(pug())
    .pipe(gulp.dest(config.prodDir))
)

gulp.task('clean:prod', () => clean([
  `${config.prodDir}${config.cssDir}**/*.map`,
  `${config.prodDir}${config.cssMainFileDir + config.cssMainFileName}.css.map`,
  `${config.prodDir}${config.imgDir}*.*`
]));

gulp.task('imagemin', () => gulp.src(`${config.devDir}${config.imgSourceDir}*.{jpg,gif}`)
  .pipe(newer(`${config.prodDir}${config.imgDir}`))
  .pipe(imagemin())
  .pipe(gulp.dest(`${config.prodDir}${config.imgDir}`)));

gulp.task('pngmin', () => gulp.src(`${config.devDir}${config.imgSourceDir}*.png`)
    .pipe(newer(`${config.prodDir}${config.imgDir}`))
    .pipe(pngmin())
    .pipe(gulp.dest(`${config.prodDir}${config.imgDir}`)));

// compiling for development (sourcemaps=true)
gulp.task('sass:dev', () => gulp.src(`${config.devDir}${config.sassDir}**/*.scss`)
  .pipe(wait(500))
  .pipe(sourcemaps.init())
  .pipe(sass().on('error', sass.logError))
  .pipe(postcss(postCssPlugins))
  .pipe(sourcemaps.write('', { includeContent: false, sourceRoot: `${config.prodDir}${config.cssDir}` }))
  .pipe(csscomb())
  .pipe(gulp.dest(`${config.prodDir}${config.cssDir}`)));

// compiling for production (sourcemaps=false)
gulp.task('sass:prod', () => gulp.src(`${config.devDir}${config.sassDir}**/*.scss`)
  .pipe(sass().on('error', sass.logError))
  .pipe(gcmq())
  .pipe(postcss(postCssPlugins))
  .pipe(csscomb())
  .pipe(gulp.dest(`${config.prodDir}${config.cssDir}`)));

gulp.task('copy', () => {
  wait(500);
  gulp.src(`${config.devDir}${config.imgSourceDir}*`)
    .pipe(newer(`${config.prodDir}${config.imgDir}`))
    .pipe(copy())
    .pipe(gulp.dest(`${config.prodDir}${config.imgDir}`));
  gulp.src(`${config.devDir}${config.jsDir}**/*.js`)
    .pipe(newer(`${config.prodDir}${config.jsDir}`))
    .pipe(copy())
    .pipe(gulp.dest(`${config.prodDir}${config.jsDir}`));
});

// Static Server + watching html/scss/js files
gulp.task('serve', ['sass:dev'], () => {
  browserSync.init({
    port: 3333,
    server: `./${config.prodDir}`
  });
  // without browserReload
  gulp.watch(`${config.devDir}${config.sassDir}**/*.scss`, ['sass:dev']);
  gulp.watch(`${config.devDir}${config.imgSourceDir}**/*.{jpg,gif}`, ['imagemin', 'copy']);
  gulp.watch(`${config.devDir}${config.imgSourceDir}**/*.png`, ['pngmin']);
  gulp.watch(`${config.devDir}${config.imgSourceDir}**/*.svg`, ['copy']);
  gulp.watch(`${config.devDir}${config.jsDir}**/*.js`, ['copy']);

  // with browserReload
  gulp.watch(`${config.devDir}**/*.pug`, ['views:compile', browserSync.reload]);
  gulp.watch(`${config.prodDir}${config.cssDir}**/*.css`).on('change', browserSync.reload);
  gulp.watch(`./${config.prodDir}*.html`).on('change', browserSync.reload);
  gulp.watch(`./${config.prodDir}${config.jsDir}*.js`).on('change', browserSync.reload);
  gulp.watch(`${config.prodDir}${config.imgDir}**`).on('change', browserSync.reload);
});

gulp.task('default', ['views:compile', 'copy', 'serve']);
gulp.task('dist', ['views:compile', 'clean:prod', 'sass:prod', 'imagemin', 'pngmin', 'copy']);
